const file = require('./../problem/Invert');

const testObject = {
    name: 'Bruce Wayne',
    age: 36,
    location: 'Gotham'
};
let obj = file.invert1(testObject);

//you can console the obj to see the new object 
console.log(obj);