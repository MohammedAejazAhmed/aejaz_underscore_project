var _ = require('./../node_modules/underscore');

function cb(value, key) {
    return value + 5;
}

function mapobject1(obj, cb) {
    return _.mapObject(obj, function (val, key) {
        return val + 5;
    });
}

module.exports = {
    cb,
    mapObject
}