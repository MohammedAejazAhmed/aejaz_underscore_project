var _ = require('./../node_modules/underscore');

function invert1(obj) {
    return _.invert(obj);
}

module.exports = {
    invert1
}