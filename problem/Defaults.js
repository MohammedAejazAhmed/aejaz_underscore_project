let _ = require('./../node_modules/underscore');

extraProperties = {
    Occupation: "Entrepeuner",
    MaritalStatus: "Single",
    Nation: "United States"
}

function Defaults1(object, defaultProps) {
    return _.defaults(object, defaultProps);
}

module.exports = {

    extraProperties,
    Defaults1

}